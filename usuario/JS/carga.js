function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}
var loc = window.location.origin+'/asistentevirtual/usuario/configuracion.json';
var json_cargado = {};
readTextFile(loc, function(text){
	json_cargado = JSON.parse(text);
});